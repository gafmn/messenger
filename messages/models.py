from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Message(models.Model):
    sender = models.ForeignKey(
        User,
        related_name='sender',
        on_delete=models.CASCADE,
    )
    reciever = models.ForeignKey(
        User,
        related_name='reciever',
        on_delete=models.CASCADE,
    )
    creation_date = models.DateTimeField()
    text = models.TextField()

    is_edited = models.BooleanField(default=False)

    def __str__(self):
        return self.text
