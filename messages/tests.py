from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from django.contrib.auth import get_user_model
from .models import Message


User = get_user_model()


class SimpleMessengetTestCase(APITestCase):

    def test_creation(self):
        '''
        Creational test
        '''
        url = '/messages/register'
        data1 = {
            'username': 'test',
            'password': 'test',
            'first_name': 'Test',
            'last_name': 'Test'
        }
        response1 = self.client.post(url, data1, format='json')
        self.assertEqual(response1.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'test')

        data2 = {
            'username': 'testik',
            'password': 'testik',
            'first_name': 'Test',
            'last_name': 'Mini'
        }
        response2 = self.client.post(url, data2, format='json')
        self.assertEqual(response2.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)

    def test_send_message(self):
        '''
        User with username `test` send message to User with username
        `testik`
        '''
        self.test_creation()
        url = '/messages/send_message'
        data = {
            'reciever_username': 'testik',
            'text': 'Hello, it is testing message!'
        }
        sender_user = User.objects.get(username='test')
        self.assertEqual(sender_user.last_name, 'Test')

        # Get Token of sender
        token = Token.objects.get(user=sender_user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        self.assertNotEqual(token, None)
        response = client.post(
            url,
            data,
            format='json',
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_edit_message(self):
        '''
        User with username `test` edit previous message

        'Hello, it is testing message!' -> 'Hello'
        '''
        # Call previous test
        self.test_send_message()

        sender_user = User.objects.get(username='test')
        message = Message.objects.filter(sender=sender_user.id)[0]

        url = f'/messages/{message.id}/edit_message'

        data = {
            'new_text': 'Hello'
        }

        # Get Token of sender
        token = Token.objects.get(user=sender_user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        self.assertNotEqual(token, None)

        response = client.post(
            url,
            data,
            format='json',
        )

        # Check that message already chenged
        message = Message.objects.get(id=message.id)
        self.assertEqual(message.text, 'Hello')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(message.is_edited, True)

    def test_read_messages(self):
        '''
        Read all messages of User with username `testik`
        '''
        # Call previous test: edtion of message
        self.test_edit_message()

        reciever_user = User.objects.get(username='testik')

        # Get Token of reciever
        token = Token.objects.get(user=reciever_user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        self.assertNotEqual(token, None)

        url = '/messages/read_messages'
        response = client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_read_last_message(self):
        '''
        Read last message of User with username `testik`
        '''
        # Call previous tests
        self.test_read_messages()

        reciever_user = User.objects.get(username='testik')

        # Get Token of reader
        token = Token.objects.get(user=reciever_user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        # Check that last message has text 'Hello'
        url = '/messages/last_recieved_message'
        response = client.get(url)
        self.assertEqual(response.data['text'], 'Hello')

    def test_delete_message(self):
        '''
        Delete message of sender
        '''
        # Call previous test: read all messages
        self.test_read_messages()
        sender_user = User.objects.get(username='test')

        # Get Token of sender
        token = Token.objects.get(user=sender_user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        self.assertNotEqual(token, None)

        # Get Message that want to delete
        message = Message.objects.get(text='Hello')
        url = f'/messages/{message.id}/delete_message'
        response = client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Check that reciever cannot read last message
        reciever_user = User.objects.get(username='testik')

        # Get token of reciever
        token = Token.objects.get(user=reciever_user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        self.assertNotEqual(token, None)

        url = '/messagess/last_recieved_message'

        response = client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
