from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework import status

from django.contrib.auth import get_user_model

from .models import Message
from .serializers import MessageSerializer, UserSerializer

import json
import logging
import datetime

# Logger
logger = logging.getLogger(__name__)

# Get standard Django `User`
User = get_user_model()


@api_view(['POST'])
def send_message(request, format=None):
    '''
    Create message and send to another user
    '''
    try:
        # Get request body data
        data = request.data
        sender = request.user

        # Get reciever username
        reciever_username = data['reciever_username']

        logger.info('Reciever username: ', reciever_username)
    except KeyError:
        raise Exception('Decoding a JSON has failed')

    try:
        # Get `User` instances
        reciever = User.objects.get(username=reciever_username)
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # Get reciever and sender id
    reciever_id = reciever.id
    sender_id = sender.id

    # Get text of message
    text = data['text']

    # Dump data
    logger.info('Dump data response...')
    message_data = {}
    message_data['sender'] = sender_id
    message_data['reciever'] = reciever_id
    message_data['creation_date'] = str(datetime.datetime.now())
    message_data['text'] = text
    message_data['is_edited'] = False
    json_data = json.loads(json.dumps(message_data))
    logger.info('done')

    logger.info('Create message...')
    serializer = MessageSerializer(data=json_data)

    if serializer.is_valid():
        serializer.save()

        logger.info('done')

        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def read_messages(request, format=None):
    '''
    Read all messages of user
    '''
    try:
        # Get `User` instance
        logger.info('Get `User` instance...')
        reader = request.user
        logger.info('done')
    except User.DoesNotExist:
        raise Exception('User with this username does not exist')

    try:
        # Get `Message` instances which reciever is current `reader`
        logger.info(f'Get messages of {reader.username}...')
        messages = Message.objects.filter(reciever=reader.id)
        serializer = MessageSerializer(messages, many=True)
        logger.info('done')

        response = []

        # Iterate through all messages data
        for item in serializer.data:
            sender = User.objects.get(id=item['sender'])
            response.append(
                {
                    'username': sender.username,
                    'text': item['text'],
                    'date': item['creation_date'],
                    'message_id': item['id'],
                }
            )

        return Response(response, status=status.HTTP_200_OK)
    except KeyError:
        raise Exception('Incorrect request body')


@api_view(['POST'])
def edit_message(request, message_id, format=None):
    '''
    Edit message that sender was send via message id
    '''
    # Get request data
    data = request.data
    sender = request.user

    try:
        # Get message that want to edit
        message = Message.objects.get(id=message_id)

        if sender.id != message.sender_id:
            raise Exception('Permission denied!')

        # Specify new text of message
        message.text = data['new_text']
        message.is_edited = True
        message.save()

        # Update message
        serializer = MessageSerializer(message)

        return Response(serializer.data, status=status.HTTP_200_OK)
    except KeyError:
        raise Exception('Incorrect request body')


@api_view(['GET'])
def last_recieved_message(request, format=None):
    '''
    Read last message of user
    '''
    # Get request data
    reader = request.user

    # Get all messages of user
    messages = Message.objects.filter(reciever_id=reader.id)

    # Check if there no messages for user
    if len(messages) == 0:
        raise Exception('User does not have any recieved message')

    # Get last message
    message = messages.last()

    if message.reciever_id != reader.id:
        raise Exception('Permission denied!')

    serializer = MessageSerializer(message)

    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['DELETE'])
def delete_message(request, message_id, format=None):
    '''
    Delete message that user was sent via message id
    '''
    # Get request data
    sender = request.user

    # Get message that want to delete
    message = Message.objects.get(id=message_id)

    if message.sender_id != sender.id:
        raise Exception('Permission denied!')

    message.delete()
    return Response(
        {
            'info': 'Message was successfully deleted!',
        },
        status=status.HTTP_204_NO_CONTENT
    )


@api_view(['POST'])
def register(request, format=None):
    '''
    Register user via username, password, name and surname
    '''
    # Get request data
    data = request.data

    try:
        logger.info('Create user...')
        serializer = UserSerializer(data=data)

        if serializer.is_valid():
            # Create user
            user = serializer.save()
            logger.info('done')

            # Create Token for user
            logger.info('Generate token...')
            token = Token.objects.create(user=user)
            logger.info('done')

            return Response(
                {'token': token.key},
                status=status.HTTP_201_CREATED,
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST,
        )

    except KeyError:
        logger.error('Key does not exist')


@api_view(['GET'])
def get_token(request, format=None):
    '''
    Get user token
    '''
    # Get request data
    data = request.data

    try:
        logger.info('Find user...')

        # Get user via username
        user = User.objects.get(username=data['username'])

        # Check password matching
        if not user.check_password(data['password']):
            raise Exception('Password does not match')

        # Get token of user
        token = Token.objects.get(user=user)
        return Response(
            {
                'token': token.key,
            },
            status=status.HTTP_200_OK
        )
    except User.DoesNotExist:
        logger.error('User does not exist')
    except KeyError:
        logger.error('Incorrect request body')
