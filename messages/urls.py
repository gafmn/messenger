from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views


urlpatterns = [
    path('send_message', views.send_message),
    path('read_messages', views.read_messages),
    path('last_recieved_message', views.last_recieved_message),
    path('<int:message_id>/edit_message', views.edit_message),
    path('<int:message_id>/delete_message', views.delete_message),
    path('register', views.register),
    path('token', views.get_token),
]

urlpatterns = format_suffix_patterns(urlpatterns)
