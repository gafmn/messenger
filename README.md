# Messenger

# How to run
    
```
docker-compose up -d
```

## Authorization

For clients to authenticate, the token key should be included in the **Authorization** HTTP header.

```
Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b
```

# Endpoints

### Register user

```
/messages/register
```

JSON **POST** request format

```
{
    "username": "username",
    "password": "password",
    "first_name": "First name",
    "last_name": "Last name"
}
```

Expected response
```
{
    "token": "token"
}
```

### Get token of user

```
/messages/token
```

JSON **GET** request format

```
{
    "username": "username",
    "password": "password"
}
```

Expected response
```
{
    "token": "token"
}
```

### Send message

```
/messages/send_message
```

JSON **POST** request format
```
{
    "reciever_username": "username",
    "text": "text"
}
```

Expected response

```
{
    "username": "username",
    "text": "text",
    "date": "date", 
    "message_id": message_id
}
```

### Read messages

**GET** request

```
/messages/read_messages
```

Expected response

```
[
    {
        "username": "username",
        "text": "text",
        "date": "date",
        "is_edited": id_edited,
        "message_id": message_id
    }
    ...
]
```

### Read last recieved message

**GET** request

```
/messages/last_recieved_message
```

Expected response

```
{
    "username": "username",
    "text": "text",
    "date": "date",
    "is_edited": is_edited,
    "message_id": message_id
}
```

### Edit message by id

```
/messages/<int:message_id>/edit_message
```

JSON **POST** request format

```
{
    "new_text": "new_text"
}
```

Expected reponse

```
{
    "username": "username",
    "text": "new_text",
    "date": "date",
    "is_edited": true,
    "message_id": message_id
}
```

### Delete message by id
**DELETE** request
```
/messages/<int:message_id>/delete_message
```

Expected response
```
{
    "info": "Message was successfully deleted!"
}
```